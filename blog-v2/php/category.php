<?php
//! Permet l'affichage des erreurs - A ne pas commit
error_reporting(-1);

// J'intègre obligatoirement (une fois) le contenu de mon fichier de connexion à ma bdd
require_once("utils/db_connect.php");

// J'intègre obligatoirement le contenu de mon fichier de fonctions
require("utils/function.php");

// J'appelle ma fonction pour savoir si mon utilisateur est connecté
isConnected();

//? Si ma méthode de requête est POST alors j'affecte à ma variable $method le contenu de la superglobale $_POST
if ($_SERVER["REQUEST_METHOD"] == "POST") $method = $_POST;
//? Sinon j'affecte à ma variable $method le contenu de la superglobale $_GET
else $method = $_GET;

//? En fonction du paramètre "choice" de ma requête j'execute les instructions de la case correspondante
switch ($method["choice"]) {
    case "select":
        // Je récupère toutes les catégories
        $req = $db->query("SELECT * FROM categories");

        // J'affecte la totalité de mes résultats à la variable $categories
        $categories = $req->fetchAll(PDO::FETCH_ASSOC);

        // J'envoie une réponse avec un success true ainsi que les catégories
        echo json_encode(["success" => true, "categories" => $categories]);
        break;

    default:
        //! Aucune case ne correspond à mon choix
        // J'envoie une réponse avec un success false et un message d'erreur
        echo json_encode(["success" => false, "error" => "Ce choix n'existe pas"]);
        break;
}
